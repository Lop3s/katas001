console.log('Números de 1 a 20')
function oneThroughTwenty() {

    for (let n = 1; n <= 20; n++) {
      console.log(n)
    }

}
oneThroughTwenty()
console.log('--------------------------------------------------------')
console.log('Números pares de 1 a 20')
function evensToTwenty() {

    for (let a = 1; a <= 20; a++) {
      if (a % 2 === 0) {
        console.log(a)
      }
    }

}

evensToTwenty()
console.log('--------------------------------------------------------')
console.log('Números ímpares de 1 a 20')
function oddsToTwenty() {
    
    for (let b = 0; b <= 20; b++) {
      if (b % 2 !== 0) {
        console.log(b)
      }
    }
   
}

oddsToTwenty()
console.log('--------------------------------------------------------')
console.log('Números múltiplos de 5 a 100')
function multiplesOfFive() {
    
    for(let c = 5; c <= 100; c += 5) {
      console.log(c)
    }

}

multiplesOfFive()
console.log('--------------------------------------------------------')
console.log('Números quadrados')
function squareNumbers() {
    
  let e = 0
  let d = 1
  while(e < 100) {
    let a = 0
    a = d * d
    d++
    e = a
    console.log(a)
  }
  
}

squareNumbers()
console.log('--------------------------------------------------------')
console.log('Contagem regressiva')
function countingBackwards() {
    
    for(let f = 20;f > 0; f--) {
      console.log(f)
    }
 
}

countingBackwards()
console.log('--------------------------------------------------------')
console.log('Números pares de 20 a 1')
function evenNumbersBackwards() {
    
    for(let g = 20; g > 0; g--) {
      if (g % 2 === 0){
        console.log(g)
      }
    }

}

evenNumbersBackwards()
console.log('--------------------------------------------------------')
console.log('Números ímpares de 20 a 1')
function oddNumbersBackwards() {
    
    for(let h = 20; h >= 0; h--) {
      if(h % 2 !== 0) {
        console.log(h)
      }
    }
  
}

oddNumbersBackwards()
console.log('--------------------------------------------------------')
console.log('Números Múltiplos de 5 até 100')
function multiplesOfFiveBackwards() {
    
    for(let i = 100;i >= 5; i -= 5) {
      console.log(i)
    }
  
}

multiplesOfFiveBackwards()
console.log('--------------------------------------------------------')
console.log('Números quadrados de 100 a 1')
function squareNumbersBackwards() {
    
    let k = Math.pow(100, 0.5)
    while(k > 0) {
      console.log(k * k)
      k--
    }
  
}

squareNumbersBackwards()